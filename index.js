const _ = require('underscore');
const prompt = require('prompt');
/*
A tree of play, each tree element has the following values
myHands:{ left:Number, right:Number }
hisHands: { left:Number, right:Number }
winnings: Number
losings: Number
action: ["LR", "RL", "LL", "RR", "SPLIT"]
hisAction: ["LR", "RL", "LL", "RR", "SPLIT"]
plays:[next]
hash:String
parent: play
*/

var top = {
  myHand: {
    left:1,
    right: 1
  },
  hisHand: {
    left:1,
    right:1
  },
  plays: [],
  half: false
};
top.hash = hash(top);

function hash(play) {
  return `${Math.min(play.myHand.left, play.myHand.right)}:${Math.max(play.myHand.left, play.myHand.right)}:${Math.min(play.hisHand.left, play.hisHand.right)}:${Math.max(play.hisHand.left, play.hisHand.right)}:${play.half}`;
}
function loose(hand) {
  return hand.left === 5 && hand.right ===5;
}
let tags=0;
function find(play, from=top, tag) {
  if(!tag) tag = ++tags;
  if(from.tag === tag) return;
  if(from.hash === play.hash) return from;
  from.tag = tag;
  for(let i = 0 ; i < from.plays.length ; i++) {
    if(find(play, from.plays[i], tag)) return from.plays[i];
  }
  return;
}
    


function execute(hand1, hand2, action) {
  console.info(`Executing ${action} with`, hand1, hand2);
  switch(action) {
    case "LR": { if(hand1.left<5 && hand2.right<5)  
                  hand2.right += hand1.left; 
                 else 
                  return false; 
                 break;}
    case "RL": { if(hand1.right<5 && hand2.left<5)
                  hand2.left += hand1.right;
                 else
                  return false; 
                 break;}
    case "LL": { if(hand1.left<5 && hand2.left < 5)
                  hand2.left += hand1.left;
                 else
                  return false; 
                 break;}
    case "RR": { if(hand1.right<5 && hand2.right < 5) 
                  hand2.right += hand1.right;
                 else
                  return false; 
                 break;}
    case "SPLIT": { if(hand1.left === 5 && hand1.right%2===0) { hand1.left = hand1.right /2; hand1.right /= 2;} else 
                    if(hand1.right === 5 && hand1.left%2===0) { hand1.right = hand1.left /2; hand1.left /= 2;} else
                      return false;
    }
    default: throw('Unknown action');
  }
  hand2.left = Math.min(hand2.left, 5);
  hand2.right = Math.min(hand2.right, 5);
  console.info(`Result ${action} with`, hand1, hand2);
  return true;
}
function calculate(play) {
  const actions=["LR", "RL", "LL", "RR", "SPLIT"];
  actions.forEach(hisAction => {
    actions.forEach(myAction => {
      const newPlay = _.clone(play);
      newPlay.parent = play;
      newPlay.action=myAction;
      newPlay.hisAction=hisAction;
      newPlay.half = false;
      newPlay.plays = [];
      if(execute(newPlay.myHand, newPlay.hisHand, myAction)){ 
        newPlay.half = true;
        newPlay.hash = hash(newPlay);
        let previous = find(newPlay);
        if(previous) {
          play.plays.push(previous);
          let n = play; while(n) { n.losings+=previous.losings; n.winnings+=previous.winnings; n=n.parent; }
        } else if(loose(newPlay.hisHand)) {
          newPlay.win = true;
          let n = play; while(n) { n.winnings++; n=n.parent;}
          play.plays.push(newPlay);
        } else {
          if(execute(newPlay.hisHand, newPlay.myHand, hisAction)) {
            console.info('Execute hand success: ', newPlay.hisHand, newPlay.myHand, hisAction);
            newPlay.half = false;
            newPlay.hash = hash(newPlay);
            if(previous) {
              play.plays.push(previous);
              let n = play; while(n) { n.losings+=previous.losings; n.winnings+=previous.winnings; n=n.parent; }
            } else if(loose(newPlay.myHand)) {
              newPlay.loose = true;
              let n = play; while(n) { n.loosings++; n=n.parent;}
              play.plays.push(newPlay);
            } else {
              play.plays.push(newPlay);
              newPlay.winnings = newPlay.loosings = 0;
              newPlay.win = newPlay.loose = undefined;
              calculate(newPlay);
            }
          }
        }
      }
    })
  })
}

function rank(current) {
  if(current.rank) return;
  if(current.win)  current.rank = 100000;
  else if(current.loose) current.rank = -100000;
  else if(current.loosings ===0) current.rank = 100000;
  current.rank = current.winnings - current.loosings;
  current.plays.forEach(play => rank(play));
  current.plays.sort((a,b) => a.rank - b.rank);
}
calculate(top);
rank(top);

prompt.start();
const representations = ['', '|', '||', '|||' ,'||||', 'O'];
function play(current) {
  console.info('Me: ', representations[current.myHand.left], '    ', representations[current.myHand.right]);
  console.info('You: ', representations[current.hisHand.left], '    ', representations[current.hisHand.right]);
  // Get the child that wins, or one that does not loose, or the one with more winnings than loosings
  let next = current.plays[0];
  console.info('I\'m playing ', next.myAction, '[', next,']');
  const newPlay = _.clone(current);
  execute(newPlay.myHand, newPlay.hisHand, next.myAction);
  console.info('Me: ', representations[newPlay.myHand.left], '    ', representations[newPlay.myHand.right]);
  console.info('You: ', representations[newPlay.hisHand.left], '    ', representations[newPlay.hisHand.right]);
  if(next.win) {
    console.info('I win !');
    process.exit(0);
  } 
  prompt.get({properties: {action: {
    pattern: /LR|RL|LL|RR|SPLIT/,
    message: "What do you want to do ?",
    require: true
  }}}, res=>{
    const child = _.where(current, {myAction: next.myAction, hisAction:result.action})[0];
    if(!child) {
      console.info('Invalid play');
      play(child);
    } else {
      if(child.loose) {
        console.info('I loose !');
        process.exit(0);
      }
      play(child);
    }
  });
}
play(top);
